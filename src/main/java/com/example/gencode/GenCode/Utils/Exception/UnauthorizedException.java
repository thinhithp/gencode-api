package com.example.gencode.GenCode.Utils.Exception;

public class UnauthorizedException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public UnauthorizedException(String message) {
		super(message);
	}

	// Other constructors or methods if needed
}