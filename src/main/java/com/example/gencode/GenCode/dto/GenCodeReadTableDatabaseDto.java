package com.example.gencode.GenCode.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenCodeReadTableDatabaseDto {
	private List<String> table;
	private String url;
	private String useName;
	private String password;

	public String getUrl() {
		if (url != null && url.startsWith("jdbc:sqlserver")) {
			// Check if the URL already has other parameters
			if (url.contains(";")) {
				// Check for trailing semicolon
				if (url.endsWith(";")) {
					return url + "encrypt=false";
				} else {
					return url + ";encrypt=false";
				}
			} else {
				return url + ";encrypt=false";
			}
		}
		return url;
	}

}
