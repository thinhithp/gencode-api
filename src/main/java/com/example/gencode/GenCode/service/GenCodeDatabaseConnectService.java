package com.example.gencode.GenCode.service;

import com.example.gencode.GenCode.dto.GenCodeColumnDetailDto;
import com.example.gencode.GenCode.dto.GenCodeReadTableDatabaseDto;
import lombok.Getter;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Service
@ComponentScan
public class GenCodeDatabaseConnectService {
	@Getter
	private Set<String> validTableNames = new HashSet<>();

	public GenCodeDatabaseConnectService(DataSource dataSource) {
		loadValidTableNames(dataSource);
	}

	public Map<String, GenCodeColumnDetailDto> getColumnDetails(GenCodeReadTableDatabaseDto baseQuerry)
			throws SQLException {
		Map<String, GenCodeColumnDetailDto> columnDetails = new HashMap<>();

		DataSource dataSource = this.createDataSource(baseQuerry.getUrl(), baseQuerry.getUseName(),
				baseQuerry.getPassword());
		String databaseProductName;
		try (Connection conn = dataSource.getConnection()) {
			DatabaseMetaData metaData = conn.getMetaData();
			databaseProductName = metaData.getDatabaseProductName();
		}

		for (String tableName : baseQuerry.getTable()) {
			if (!isValidTableName(tableName)) {
				throw new IllegalArgumentException("Invalid table name: " + tableName);
			}

			String query = buildCombinedQuery(databaseProductName, tableName);

			try (Connection conn = dataSource.getConnection(); PreparedStatement stmt = conn.prepareStatement(query)) {
				// Thiết lập tham số cho PreparedStatement
				setQueryParameters(stmt, databaseProductName, conn, tableName);

				try (ResultSet rs = stmt.executeQuery()) {
					while (rs.next()) {
						String columnName = rs.getString("column_name");
						String defaultValue = getDefaultValueFromResultSet(rs, databaseProductName);
						String description = getDescriptionFromResultSet(rs, databaseProductName);

						GenCodeColumnDetailDto detail = new GenCodeColumnDetailDto();
						detail.setColumnName(columnName);
						detail.setDefaultValue(defaultValue);
						detail.setDescription(description);

						// Create a unique key for each column by combining table name and column name
						String uniqueKey = tableName + "." + columnName;
						columnDetails.put(uniqueKey, detail);
					}
				}
			}
		}
		return columnDetails;
	}

	public DataSource createDataSource(String url, String username, String password) {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setUrl(url);
		dataSource.setUsername(username);
		dataSource.setPassword(password);
		// Đặt thêm thuộc tính khác cho dataSource nếu cần
		loadValidTableNames(dataSource);
		return dataSource;
	}

	private boolean isValidTableName(String tableName) {
		return validTableNames.contains(tableName.toLowerCase());
	}

	private String buildCombinedQuery(String databaseProductName, String tableName) {
		switch (databaseProductName) {
		case "PostgreSQL":
			return "SELECT cols.column_name, "
					+ "pg_catalog.col_description(c.oid, cols.ordinal_position::int) AS column_description, "
					+ "cols.column_default " + "FROM information_schema.columns cols "
					+ "LEFT JOIN pg_catalog.pg_class c ON c.relname = cols.table_name "
					+ "WHERE cols.table_schema = 'public' AND cols.table_name = ?";
		case "MySQL":
			return "SELECT COLUMN_NAME, COLUMN_COMMENT, COLUMN_DEFAULT " + "FROM information_schema.COLUMNS "
					+ "WHERE TABLE_SCHEMA = ? AND TABLE_NAME = ?";
		case "Microsoft SQL Server":
			return "SELECT c.name AS COLUMN_NAME, ep.value AS COLUMN_DESCRIPTION, dc.definition AS COLUMN_DEFAULT "
					+ "FROM sys.columns c "
					+ "LEFT JOIN sys.default_constraints dc ON c.default_object_id = dc.object_id "
					+ "LEFT JOIN sys.extended_properties ep ON ep.major_id = c.object_id AND ep.minor_id = c.column_id AND ep.name = 'MS_Description' "
					+ "JOIN sys.tables t ON c.object_id = t.object_id " + "WHERE t.name = ?";
		case "Oracle":
			return "SELECT col.column_name, com.comments AS COLUMN_DESCRIPTION, col.data_default AS COLUMN_DEFAULT "
					+ "FROM all_tab_columns col "
					+ "LEFT JOIN all_col_comments com ON col.table_name = com.table_name AND col.column_name = com.column_name "
					+ "WHERE col.table_name = ?";
		// Add cases for other DBMS as needed
		default:
			throw new UnsupportedOperationException("Unsupported database: " + databaseProductName);
		}
	}

	public void loadValidTableNames(DataSource dataSource) {
		try (Connection conn = dataSource.getConnection();
				ResultSet rs = conn.getMetaData().getTables(null, null, "%", new String[] { "TABLE" })) {
			while (rs.next()) {
				validTableNames.add(rs.getString("TABLE_NAME").toLowerCase());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void setQueryParameters(PreparedStatement stmt, String databaseProductName, Connection conn,
			String tableName) throws SQLException {
		if (databaseProductName.equals("MySQL")) {
			stmt.setString(1, conn.getCatalog());
			stmt.setString(2, tableName);
		} else {
			stmt.setString(1, tableName);
		}
	}

	public String getDefaultValueFromResultSet(ResultSet rs, String databaseProductName) throws SQLException {
		switch (databaseProductName) {
		case "PostgreSQL":
			return rs.getString("column_default");
		case "MySQL":
			return rs.getString("COLUMN_DEFAULT"); // Tên cột phù hợp với MySQL
		case "Oracle":
			return rs.getString("COLUMN_DEFAULT"); // Tên cột phù hợp với Oracle, sử dụng DATA_DEFAULT hoặc tương tự
		case "Microsoft SQL Server":
			return rs.getString("COLUMN_DEFAULT"); // Tên cột phù hợp với SQL Server, sử dụng definition hoặc tương tự
		// Thêm các trường hợp cho các DBMS khác nếu cần
		default:
			return null;
		}
	}

	public String getDescriptionFromResultSet(ResultSet rs, String databaseProductName) throws SQLException {
		switch (databaseProductName) {
		case "PostgreSQL":
			return rs.getString("column_description");
		case "MySQL":
			return rs.getString("COLUMN_COMMENT");
		case "Microsoft SQL Server":
			return rs.getString("column_description"); // Ho
		// Thêm các case khác nếu cần truy vấn nhiều loại Db
		default:
			return null;
		}
	}

	public Map<String, String> getForeignKeys(Connection conn, String table) throws SQLException {
		Map<String, String> foreignKeys = new HashMap<>();
		DatabaseMetaData metaData = conn.getMetaData();
		try (ResultSet fkResultSet = metaData.getImportedKeys(conn.getCatalog(), null, table)) {
			while (fkResultSet.next()) {
				String fkColumnName = fkResultSet.getString("FKCOLUMN_NAME");
				String pkTableName = fkResultSet.getString("PKTABLE_NAME");
				String pkColumnName = fkResultSet.getString("PKCOLUMN_NAME");
				foreignKeys.put(fkColumnName, pkTableName + "(" + pkColumnName + ")");
			}
		}
		return foreignKeys;
	}

}
