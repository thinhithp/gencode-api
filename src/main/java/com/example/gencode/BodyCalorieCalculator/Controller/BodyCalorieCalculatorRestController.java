package com.example.gencode.BodyCalorieCalculator.Controller;

import com.example.gencode.BodyCalorieCalculator.BodyCalorieCalculatorDto.BodyCalorieCalculatorDto;
import com.example.gencode.BodyCalorieCalculator.Service.BodyCalorieCalculatorService;
import com.example.gencode.BodyCalorieCalculator.UserDto;
import com.example.gencode.GenCode.Utils.ApiConstants;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(ApiConstants.BASE + ApiConstants.BodyCalculator.BASE)
@CrossOrigin("*")
public class BodyCalorieCalculatorRestController {
	@Autowired
	private BodyCalorieCalculatorService service;

	@GetMapping("/calculator")
	public BodyCalorieCalculatorDto bodyCalculator(@ModelAttribute UserDto userDto) {
		return service.totalCalculate(userDto);
	}
}
