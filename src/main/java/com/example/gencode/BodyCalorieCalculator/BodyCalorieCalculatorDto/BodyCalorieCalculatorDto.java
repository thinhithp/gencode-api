package com.example.gencode.BodyCalorieCalculator.BodyCalorieCalculatorDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class BodyCalorieCalculatorDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<String> bodyCalculate;

	private List<String> bodyFat;
}
