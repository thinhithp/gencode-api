package com.example.gencode.BodyCalorieCalculator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Comment;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
	@Comment(value = "Chiều cao")
	private Integer height;

	@Comment(value = "Tuổi")
	private Integer age;

	@Comment(value = "Cân nặng")
	private Double weight;

	@Comment(value = "Giới tính")
	private String gender;

	@Comment(value = "Giờ tập luyện")
	private Double exerciseHours;

	@Comment(value = "Mức độ hoạt động")
	private Integer activityLevel;

	public double getHeightInMeters() {
		return height / 100.0;
	}
}
