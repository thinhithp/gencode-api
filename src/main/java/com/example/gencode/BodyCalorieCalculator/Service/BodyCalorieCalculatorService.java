package com.example.gencode.BodyCalorieCalculator.Service;

import com.example.gencode.BodyCalorieCalculator.BodyCalorieCalculatorDto.BodyCalorieCalculatorDto;
import com.example.gencode.BodyCalorieCalculator.UserDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BodyCalorieCalculatorService {

	public BodyCalorieCalculatorDto totalCalculate(UserDto userDto) {
		BodyCalorieCalculatorDto bodyCalorieCalculatorDto = new BodyCalorieCalculatorDto();
		bodyCalorieCalculatorDto.setBodyCalculate(
				List.of(calculateFatIntake(userDto), calculateCaloriesNeededForWeightLoss(userDto)));
		bodyCalorieCalculatorDto.setBodyFat(calculateExcessFatTotal(userDto));
		return bodyCalorieCalculatorDto;
	}

	private String calculateFatIntake(UserDto userDto) {
		double bmr;
		double activityFactor;
		// Định nghĩa hệ số hoạt động dựa trên mức độ tập luyện
		activityFactor = getActivityFactor(userDto);

		// Tính BMR dựa trên giới tính
		bmr = getBmr(userDto);

		// Tính TDEE
		double tdee = bmr * activityFactor;

		// Tính lượng calo từ chất béo (giả sử 30% tổng calo)
		double caloriesFromFat = tdee * 0.30;

		// Chuyển đổi calo từ chất béo sang gram (1 gram chất béo = 9 calo)
		double calculator = Math.round((caloriesFromFat / 9) * 100.0) / 100.0;

		return "Lượng chất béo cần thiết hàng ngày (gram): " + calculator;
	}

	private List<String> calculateExcessFatTotal(UserDto userDto) {
		// tinh BMI
		double bmi = Math.round((this.calculateBMI(userDto.getWeight(), userDto.getHeightInMeters())) * 100.0) / 100.0;
		double bodyFatPercentage =
				Math.round((this.estimateBodyFatPercentage(bmi, userDto.getAge(), userDto.getGender())) * 100.0)
						/ 100.0;
		double excessFat =
				Math.round((this.calculateExcessFat(userDto.getWeight(), bodyFatPercentage)) * 100.0) / 100.0;
		String bmiReturnText = "Chỉ số BMI: ".concat(String.valueOf(bmi));
		String bodyFatPercentageText = "Ước lượng tỷ lệ phần trăm mỡ cơ thể: ".concat(
				String.valueOf(bodyFatPercentage));
		String excessFatText = "Lượng mỡ dư thừa: ".concat(String.valueOf(excessFat)).concat(" kg");
		return List.of(bmiReturnText, bodyFatPercentageText, excessFatText);
	}

	private String calculateCaloriesNeededForWeightLoss(UserDto userDto) {
		double tdee;
		final double calorieDeficitPerDay = 1100; // Calorie deficit to lose 1 kg per week
		// Định nghĩa hệ số hoạt động dựa trên mức độ tập luyện
		double activityFactor = getActivityFactor(userDto);
		double bmr = getBmr(userDto);
		tdee = bmr * activityFactor;
		double caloriesNeededForWeightLoss = Math.round((tdee - calorieDeficitPerDay) * 100.0) / 100.0;
		return "Lượng calo cần thiết để giảm cân mỗi ngày: " + caloriesNeededForWeightLoss + " (calo)";
	}

	// Ước lượng tỷ lệ phần trăm mỡ cơ thể
	private double estimateBodyFatPercentage(double bmi, int age, String gender) {
		double bodyFatPercentage;
		if ("nam".equalsIgnoreCase(gender)) {
			bodyFatPercentage = (1.20 * bmi) + (0.23 * age) - 16.2;
		} else {
			bodyFatPercentage = (1.20 * bmi) + (0.23 * age) - 5.4;
		}
		return bodyFatPercentage;
	}

	// Tính BMI
	private double calculateBMI(double weight, double height) {
		return weight / (height * height);
	}

	// Tính lượng mỡ dư thừa
	private double calculateExcessFat(double weight, double bodyFatPercentage) {
		return (bodyFatPercentage / 100) * weight;
	}

	private double getBmr(UserDto userDto) {
		double bmr;
		if (userDto.getGender().equalsIgnoreCase("nam")) {
			bmr = 88.362 + (13.397 * userDto.getWeight()) + (4.799 * userDto.getHeightInMeters()) - (5.677
					* userDto.getAge());
		} else {
			bmr = 447.593 + (9.247 * userDto.getWeight()) + (3.098 * userDto.getHeightInMeters()) - (4.330
					* userDto.getAge());
		}
		return bmr;
	}

	private double getActivityFactor(UserDto userDto) {
		double activityFactor;
		switch (userDto.getActivityLevel()) {
		case 1:
			activityFactor = 1.2;
			break;
		case 2:
			activityFactor = 1.55;
			break;
		case 3:
			activityFactor = 1.725;
			break;
		default:
			activityFactor = 1.2; // Mặc định nếu không khớp với bất kỳ trường hợp nào
		}
		return activityFactor;
	}
}
