package com.example.gencode.WebSocketConfig;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketController {
	@MessageMapping("/message")
	@SendTo("/topic/messages")
	public ChatMessage sendMessage(@Payload ChatMessage chatMessage) {
		return chatMessage;
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	// ChatMessage class
	public static class ChatMessage {
		private String sender;
		private String content;
		private long timestamp;

		// getters and setters
	}
}
