CREATE OR REPLACE FUNCTION create_tag_san_pham(IN p_san_pham_id int8, IN p_tag_id int8, OUT v_cursor refcursor)
LANGUAGE plpgsql
AS $function$
BEGIN
    INSERT INTO tag_san_pham (san_pham_id, tag_id) VALUES (p_san_pham_id, p_tag_id);
OPEN v_cursor for SELECT * FROM tag_san_pham WHERE ID = p_id;
END;
$function$;
----------------------- UPDATE -------------------------------------
CREATE OR REPLACE FUNCTION update_tag_san_pham(IN p_san_pham_id int8, IN p_tag_id int8, OUT v_cursor refcursor)
LANGUAGE plpgsql
AS $function$
BEGIN
    UPDATE tag_san_pham SET tag_id = p_tag_id WHERE san_pham_id = p_san_pham_id;
OPEN v_cursor for SELECT * FROM tag_san_pham WHERE san_pham_id = p_san_pham_id;
END;
$function$;
---------------------------DETAIL----------------------
CREATE OR REPLACE FUNCTION view_tag_san_pham(IN p_id INTEGER, OUT v_cursor refcursor)
LANGUAGE plpgsql
AS $function$
BEGIN
    OPEN v_cursor FOR SELECT san_pham_id, tag_id FROM tag_san_pham WHERE ID = p_id;
END;
$function$;
--------------------DELETE---------------------
CREATE OR REPLACE FUNCTION delete_tag_san_pham(IN p_id INTEGER, OUT v_cursor refcursor)
LANGUAGE plpgsql
AS $function$
BEGIN
    DELETE FROM tag_san_pham WHERE ID = p_id;
END;
$function$;
